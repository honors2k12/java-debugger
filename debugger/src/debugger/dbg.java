package debugger;
import java.util.Arrays;
import java.util.List;




//import process.BY_HANDLE_FILE_INFORMATION;



import com.sun.jna.ptr.IntByReference;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.Union;
import com.sun.jna.platform.win32.WTypes.LPSTR;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinBase.SYSTEM_INFO;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32;
import com.sun.jna.platform.win32.WinDef.LPVOID;
import com.sun.jna.platform.win32.WinDef.PVOID;
import com.sun.jna.platform.win32.WinNT.HANDLE;

public class dbg {
	public static final int PROCESS_QUERY_INFORMATION = 0x0400;
	public static final int PROCESS_VM_READ = 0x0010;
	public static final int PROCESS_VM_WRITE = 0x0020;
	public static final int PROCESS_VM_OPERATION = 0x0008;
	//public static final int PROCESS_ALL_ACCESS = ( 0x000F0000 | 0x00100000 | 0xFFF);
	
	public static final int TH32CS_SNAPHEAPLIST = 0x00000001;
	public static final int TH32CS_SNAPPROCESS  = 0x00000002;
	public static final int	TH32CS_SNAPTHREAD   = 0x00000004;
	public static final int TH32CS_SNAPMODULE   = 0x00000008;
	public static final int	TH32CS_INHERIT      = 0x80000000;
	public static final int TH32CS_SNAPALL      = (TH32CS_SNAPHEAPLIST | TH32CS_SNAPPROCESS | TH32CS_SNAPTHREAD | TH32CS_SNAPMODULE);

	
	//Used in Thread32First Function to retrieve information about the first thread of any process encountered in a system snapshot.
	//Used in Thread32Next Function to retrieve information about the next thread of any process encountered in the system memory snapshot.
//	static Kernel32 kernel32 = (Kernel32) Native.loadLibrary("kernel32", Kernel32.class);
	
	static class Kernel32
	{
	     static
	     {
	          Native.register("kernel32");
	     }
	     public static native Pointer OpenProcess(int dwDesiredAccess, boolean bInheritHandle, int dwProcessId);
	     public static native boolean CloseHandle(Pointer hObject);
	 //    public static native boolean GetFileInformationByHandle(
	   //           Pointer hFile,
	     //         BY_HANDLE_FILE_INFORMATION lpFileInformation
	       //     );
	     public static native int  GetProcessId(  Pointer Process);
	     public static native boolean GetProcessTimes(Pointer handle, FILETIME lpCreationTime, FILETIME lpExitTime, FILETIME lpKernelTime, FILETIME lpUserTime);
	     public static native void GetSystemInfo(SYSTEM_INFO lpSystemInfo);
	     public static native boolean ReadProcessMemory(Pointer hProcess,int lpBaseAddress,Pointer lpBuffer,int nSize,IntByReference lpNumberOfBytesRead);
	     //To change above later
	     public static native boolean WriteProcessMemory(Pointer hProcess,int lpBaseAddress,Pointer lpBuffer,int nSize,IntByReference lpNumberOfBytesRead);
	     //To change above later
	     public static native boolean VirtualProtectEx(Pointer hProcess,int lpAddress,int dwSize,int flNewProtect,IntByReference lpflOldProtect);
	     public static native boolean DebugActiveProcess(int dwProcessId);
	     public static native boolean DebugSetProcessKillOnExit(boolean kill_on_exit);
	     public static native Pointer CreateToolhelp32Snapshot(int dwFlags,int th32ProcessID);
	     public static native boolean Thread32First(Pointer hSnapshot,THREADENTRY32 lpte);
	     public static native boolean Thread32Next(Pointer hSnapshot,THREADENTRY32 lpte);
	     public static native Pointer OpenThread(int dwDesiredAccess,boolean bInheritHandle,int dwThreadId);
	     public static native boolean GetThreadContext(Pointer hThread,Context lpContext);
	     public static native boolean SetThreadContext(Pointer hThread,Context lpContext);
	     public static native Pointer LoadLibraryA(String lpFileName);
	     public static native int GetProcAddress(Pointer hModule,String lpProcName);
	     public static native boolean FreeLibrary(Pointer hModule);
	     public static native boolean ContinueDebugEvent(int dwProcessId,int dwThreadId,int dwContinueStatus);
	     public static native boolean FlushInstructionCache(Pointer hProcess,Pointer lpBaseAddress,int dwSize);
	     public static native boolean WaitForDebugEvent(DEBUG_EVENT lpDebugEvent,int dwMilliseconds);
	     public static native boolean SuspendThread(Pointer h_thread);
	     public static native boolean ResumeThread(Pointer h_thread);

	}
	
	public static class THREADENTRY32 extends Structure {
	    public static class ByValue extends THREADENTRY32 implements Structure.ByValue {
	    }

	    public static class ByReference extends THREADENTRY32 implements Structure.ByReference {
	    }

	    public THREADENTRY32() {
	        dwSize = size();
	    }

	    @Override
	    protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] { "dwSize", "cntUsage", "th32ThreadID", "th32OwnerProcessID", "tpBasePri",
	                "tpDeltaPri", "dwFlags" });
	    }

	    public int dwSize;
	    public int cntUsage;
	    public int th32ThreadID;
	    public int th32OwnerProcessID;
	    public int tpBasePri;
	    public int tpDeltaPri;
	    public int dwFlags;
	}
	
	public static class FLOATING_SAVE_AREA extends Structure{
		  public static class ByValue extends FLOATING_SAVE_AREA implements Structure.ByValue {
		    }

		    public static class ByReference extends FLOATING_SAVE_AREA implements Structure.ByReference {
		    }
		    

		    @Override
		    protected List<?> getFieldOrder() {
		        return Arrays.asList(new String[] { "ControlWord", "StatusWord", "TagWord", "ErrorOffset", "ErrorSelector",
		                "DataOffset", "DataSelector","RegisterArea","Cr0NpxState" });
		    }
		public long ControlWord;
		public long StatusWord;
		public long TagWord;
		public long ErrorOffset;
		public long ErrorSelector;
		public long DataOffset;
		public long DataSelector;
		public int[] RegisterArea = new int[80];
		public long Cr0NpxState;
	}
	
	public static class EXCEPTION_DEBUG_INFO extends Structure {
		/// C type : EXCEPTION_RECORD
		public EXCEPTION_RECORD ExceptionRecord;
		public int dwFirstChance;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[] { "ExceptionRecord", "dwFirstChance"});
		}
		public static class ByReference extends EXCEPTION_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends EXCEPTION_DEBUG_INFO implements Structure.ByValue {
			
		};
	}
	
	public static class u_union extends Union {
		public EXCEPTION_DEBUG_INFO Exception;
		public CREATE_THREAD_DEBUG_INFO CreateThread;
		public CREATE_PROCESS_DEBUG_INFO CreateProcessInfo;
		public EXIT_THREAD_DEBUG_INFO ExitThread;
		public EXIT_PROCESS_DEBUG_INFO ExitProcess;
		public LOAD_DLL_DEBUG_INFO LoadDll;
		public UNLOAD_DLL_DEBUG_INFO UnloadDll;
		public OUTPUT_DEBUG_STRING_INFO DebugString;
		public RIP_INFO RipInfo;
		public static class ByReference extends u_union implements Structure.ByReference {
			
		};
		public static class ByValue extends u_union implements Structure.ByValue {
			
		};
		/*
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"Exception", "CreateThread", "CreateProcessInfo", "ExitThread","ExitProcess"
			,"LoadDll"	,"UnloadDll"	,"DebugString","RipInfo"});
		}*/
	}
	public static class DEBUG_EVENT extends Structure {
		public int dwDebugEventCode;
		public int dwProcessId;
		public int dwThreadId;
		public u_union u=new u_union();
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"dwDebugEventCode", "dwProcessId", "dwThreadId", "u"});
		}
		public static class ByReference extends DEBUG_EVENT implements Structure.ByReference {
			
		};
		public static class ByValue extends DEBUG_EVENT implements Structure.ByValue {
			
		};
	}

	
	
	
	public static class CREATE_THREAD_DEBUG_INFO extends Structure {
		public Pointer hThread;
		public int lpThreadLocalBase;
		/// C type : LPTHREAD_START_ROUTINE
		public LPTHREAD_START_ROUTINE lpStartAddress;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[] { "hThread", "lpThreadLocalBase", "lpStartAddress"});
		}
		
		public static class ByReference extends CREATE_THREAD_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends CREATE_THREAD_DEBUG_INFO implements Structure.ByValue {
			
		};
	}


	public static class CREATE_PROCESS_DEBUG_INFO extends Structure {
		public Pointer hFile;
		public Pointer hProcess;
		public Pointer hThread;
		public int lpBaseOfImage;
		public int dwDebugInfoFileOffset;
		public int nDebugInfoSize;
		public int lpThreadLocalBase;
		public LPTHREAD_START_ROUTINE lpStartAddress;
		public int lpImageName;
		public short fUnicode;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[] { "hFile", "hProcess", "hThread", "lpBaseOfImage", "dwDebugInfoFileOffset", "nDebugInfoSize", "lpThreadLocalBase", "lpStartAddress", "lpImageName", "fUnicode"});
		}
		public static class ByReference extends CREATE_PROCESS_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends CREATE_PROCESS_DEBUG_INFO implements Structure.ByValue {
			
		};
	}
	public static class LPTHREAD_START_ROUTINE extends  Structure{
		public Pointer hThread;
		public int lpThreadLocalBase;
		public LPTHREAD_START_ROUTINE.ByReference lpStartAddress;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[] { "hThread", "lpThreadLocalBase", "lpStartAddress"});
		}
		public static class ByReference extends LPTHREAD_START_ROUTINE implements Structure.ByReference {
			
		};
		public static class ByValue extends LPTHREAD_START_ROUTINE implements Structure.ByValue {
			
		};
		
	}
	public static class EXIT_THREAD_DEBUG_INFO extends Structure {
		public int dwExitCode;
		protected List<?> getFieldOrder() {
				return Arrays.asList(new String[] { "dwExitCode"});
		}
		public static class ByReference extends EXIT_THREAD_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends EXIT_THREAD_DEBUG_INFO implements Structure.ByValue {
			
		};
	}

	public static class EXIT_PROCESS_DEBUG_INFO extends Structure {
		public int dwExitCode;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[] { "dwExitCode"});
		}
		public static class ByReference extends EXIT_PROCESS_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends EXIT_PROCESS_DEBUG_INFO implements Structure.ByValue {
			
		};
	}

	public static class LOAD_DLL_DEBUG_INFO extends Structure {
		public Pointer hFile;
		public int lpBaseOfDll;
		public int dwDebugInfoFileOffset;
		public int nDebugInfoSize;
		public int lpImageName;
		public short fUnicode;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"hFile", "lpBaseOfDll", "dwDebugInfoFileOffset", "nDebugInfoSize", "lpImageName", "fUnicode"});
		}
		public static class ByReference extends LOAD_DLL_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends LOAD_DLL_DEBUG_INFO implements Structure.ByValue {
			
		};
	}

	public static class UNLOAD_DLL_DEBUG_INFO extends Structure {
		public int lpBaseOfDll;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"lpBaseOfDll"});
		}
		public static class ByReference extends UNLOAD_DLL_DEBUG_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends UNLOAD_DLL_DEBUG_INFO implements Structure.ByValue {
			
		};
	}

	public static class OUTPUT_DEBUG_STRING_INFO extends Structure {
		public String lpDebugStringData;
		public short fUnicode;
		public short nDebugStringLength;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"lpDebugStringData", "fUnicode", "nDebugStringLength"});
		}
		public static class ByReference extends OUTPUT_DEBUG_STRING_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends OUTPUT_DEBUG_STRING_INFO implements Structure.ByValue {
			
		};
	}
	
	public static class RIP_INFO extends Structure {
		public int dwError;
		public int dwType;
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"dwError", "dwType"});
		}
		public static class ByReference extends RIP_INFO implements Structure.ByReference {
			
		};
		public static class ByValue extends RIP_INFO implements Structure.ByValue {
			
		};
	}

	public static class EXCEPTION_RECORD extends Structure {
		public int ExceptionCode;
		public int ExceptionFlags;
		public EXCEPTION_RECORD.ByReference ExceptionRecord;
		public int ExceptionAddress;
		public int NumberParameters;
		public long[] ExceptionInformation = new long[15];
		protected List<?> getFieldOrder() {
			return Arrays.asList(new String[]{"ExceptionCode", "ExceptionFlags", "ExceptionRecord", "ExceptionAddress", "NumberParameters", "ExceptionInformation"});
		}
		public static class ByReference extends EXCEPTION_RECORD implements Structure.ByReference {
			
		};
		public static class ByValue extends EXCEPTION_RECORD implements Structure.ByValue {
			
		};
	}

	public  static class Bytes extends Structure{
		public byte BaseMid;
		public byte Flags1;
		public byte Flags2;
		public byte BaseHi;
	    protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] { "BaseMid", "Flags1", "Flags2", "BaseHi"});
	    }
	}

	public static  class Bits extends Structure{
		public int BaseMid;
		public int Type;
		public int Dpl;
		public int Pres;
		public int LimitHi;
		public int Sys;
		public int Reserved_0;
		public int Default_Big;
		public int Granularity;
		public int BaseHi;
		@Override
		protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] { "BaseMid", "Type", "Dpl", "Pres", "LimitHi","Sys", "Reserved_0"
	        		, "Default_Big", "Granularity", "BaseHi"});
	    }
	}
	
	public static class LDT_ENTRY extends Structure{
		
		public int LimitLow;
		public int BaseLow;
	    public _HighWord HighWord;
		 public static class ByValue extends LDT_ENTRY implements Structure.ByValue {
		    }

		 public static class ByReference extends LDT_ENTRY implements Structure.ByReference {
		    }
		    public  static class _HighWord extends Union{
		    	
		    	public Bytes Bytes;
		    	public Bits Bits;
		    	public static class ByReference extends _HighWord  implements Structure.ByReference {
					
				};
				public static class ByValue extends _HighWord implements Structure.ByValue {
					
				};
		    }
			@Override
			protected List<?> getFieldOrder() {
				return Arrays.asList(new String[] { "LimitLow", "BaseLow","HighWord"});
			}
		    
	}
	
	public static class M128A extends Structure{
		 public static class ByValue extends M128A implements Structure.ByValue {
		    }

		    public static class ByReference extends M128A implements Structure.ByReference {
		    }
		    public long Low;
		    public long High;
		    protected List<?> getFieldOrder() {
		        return Arrays.asList(new String[] { "Low", "High"});
		    }
	}
	public static class XMM_SAVE_AREA32 extends Structure{
		public int ControlWord;
		public int StatusWord;
		public byte TagWord;
		public byte Reserved1;
		public int ErrorOpcode;
		public int ErrorOffset;
		public int ErrorSelector;
		public int Reserved2;
		public int DataOffset;
		public int DataSelector;
		public int Reserved3;
		public int MxCsr;
		public int MxCsr_Mask;
		public M128A[] FloatRegisters = new M128A[8];
		public M128A[] XmmRegisters = new M128A[16];
		public byte[] Reserved4=new byte[96];
		public static class ByValue extends XMM_SAVE_AREA32 implements Structure.ByValue {
	    }

	    public static class ByReference extends XMM_SAVE_AREA32 implements Structure.ByReference {
	    }
	    protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] { "ControlWord", "StatusWord"
	        		,"TagWord","Reserved1","ErrorOpcode","ErrorOffset"
	        		,"ErrorSelector","Reserved2","DataOffset","DataSelector"
	        		,"Reserved3","MxCsr","MxCsr_Mask","FloatRegisters"
	        		,"XmmRegisters","Reserved4"});
	    }
	}
	public static class DUMMYSTRUCTNAME extends Structure{
		public static class ByValue extends DUMMYSTRUCTNAME implements Structure.ByValue {
	    }

	    public static class ByReference extends DUMMYSTRUCTNAME implements Structure.ByReference {
	    }
		public M128A[] Header = new M128A[2];
		public M128A[] Legacy = new M128A[8];
		public M128A Xmm0;
		public M128A Xmm1;
		public M128A Xmm2;
		public M128A Xmm3;
		public M128A Xmm4;
		public M128A Xmm5;
		public M128A Xmm6;
		public M128A Xmm7;
		public M128A Xmm8;
		public M128A Xmm9;
		public M128A Xmm10;
		public M128A Xmm11;
		public M128A Xmm12;
		public M128A Xmm13;
		public M128A Xmm14;
		public M128A Xmm15;
		protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] { "Header", "Legacy","Xmm0","Xmm1","Xmm2"
	        		,"Xmm3","Xmm4","Xmm5","Xmm6","Xmm7","Xmm8","Xmm9","Xmm10","Xmm11"
	        		,"Xmm12","Xmm13","Xmm14","Xmm15"});
	    }

	}
	public static class DUMMYUNIONNAME extends Union{
		public static class ByValue extends DUMMYUNIONNAME implements Structure.ByValue {
	    }

	    public static class ByReference extends DUMMYUNIONNAME implements Structure.ByReference {
	    }
	    public XMM_SAVE_AREA32 FltSave;
	    public DUMMYSTRUCTNAME DUMMYSTRUCTNAME;
	}
	public static class Context extends Structure{
		  public static class ByValue extends Context implements Structure.ByValue {
		    }

		    public static class ByReference extends Context implements Structure.ByReference {
		    }
		    public long P1Home;
		    public long P2Home;
		    public long P3Home;
		    public long P4Home;
		    public long P5Home;
		    public long P6Home;
		    
		    public int ContextFlags;
		    public int MxCsr;
		    
		    public int SegCs;
		    public int SegDs;
		    public int SegEs;
		    public int SegFs;
		    public int SegGs;
		    public int SegSs;
		    public int EFlags;
		    
		    //
		    // Debug registers
		    //

		    public long Dr0;
		    public long Dr1;
		    public long Dr2;
		    public long Dr3;
		    public long Dr6;
		    public long Dr7;

		    //
		    // Integer registers.
		    //

		    public long Rax;
		    public long Rcx;
		    public long Rdx;
		    public long Rbx;
		    public long Rsp;
		    public long Rbp;
		    public long Rsi;
		    public long Rdi;
		    public long R8;
		    public long R9;
		    public long R10;
		    public long R11;
		    public long R12;
		    public long R13;
		    public long R14;
		    public long R15;

		    //
		    // Program counter.
		    //

		    public int Rip;
		    
		    //
		    // Special debug control registers.
		    //

		    public long DebugControl;
		    public long LastBranchToRip;
		    public long LastBranchFromRip;
		    public long LastExceptionToRip;
		    public long LastExceptionFromRip;
		    
		    //
		    // Vector registers.
		    //
		    public DUMMYUNIONNAME DUMMYUNIONNAME;
		    public M128A[] VectorRegister=new M128A[26];
		    public long VectorControl;
		    
		    protected List<?> getFieldOrder() {
				return Arrays.asList(new String[] { "P1Home", "P2Home","P3Home","P4Home"
						,"P5Home","P6Home","ContextFlags","MxCsr","SegCs","SegDs","SegEs"
						,"SegFs","SegGs","SegSs","EFlags","Dr0","Dr1","Dr2","Dr3","Dr6"
						,"Dr7","Rax","Rcx","Rdx","Rbx","Rsp","Rbp","Rsi","Rdi","R8","R9"
						,"R10","R11","R12","R13","R14","R15","Rip","DebugControl",
						"LastBranchToRip","LastBranchFromRip","LastExceptionToRip"
						,"LastExceptionFromRip","DUMMYUNIONNAME","VectorRegister"
						,"VectorControl"});
			}

		    //
		    // Floating point state.
		    //
/*
		    union {
		        XMM_SAVE_AREA32 FltSave;
		        struct {
		            M128A Header[2];
		            M128A Legacy[8];
		            M128A Xmm0;
		            M128A Xmm1;
		            M128A Xmm2;
		            M128A Xmm3;
		            M128A Xmm4;
		            M128A Xmm5;
		            M128A Xmm6;
		            M128A Xmm7;
		            M128A Xmm8;
		            M128A Xmm9;
		            M128A Xmm10;
		            M128A Xmm11;
		            M128A Xmm12;
		            M128A Xmm13;
		            M128A Xmm14;
		            M128A Xmm15;
		        } DUMMYSTRUCTNAME;
		    } DUMMYUNIONNAME;
*/
	}
	/*
	public static class Context extends Structure{
		
		  public static class ByValue extends Context implements Structure.ByValue {
		    }

		    public static class ByReference extends Context implements Structure.ByReference {
		    }
		    

		    @Override
		    protected List<?> getFieldOrder() {
		        return Arrays.asList(new String[] { "ContextFlags", "Dr0", "Dr1", "Dr2", "Dr3",
		                "Dr6", "Dr7","FloatSave","SegGs","SegFs","SegEs", "SegDs","Edi","Esi"
		                ,"Ebx","Edx","Ecx","Eax","Ebp","Eip","RIP","SegCs","EFlags","Esp","ExtendedRegisters"});
		    }
		
		
		public int ContextFlags;
		public int Dr0;
		public int Dr1;
		public int Dr2;
		public int Dr3;
		public int Dr6;
		public int Dr7;
		public FLOATING_SAVE_AREA FloatSave;
		public int SegGs;
		public int SegFs;
		public int SegEs;
		public int SegDs;
		public int Edi;
		public int Esi;
		public int Ebx;
		public int Edx;
		public int Ecx;
		public int Eax;
		public int Ebp;
		public int Eip;
		public int RIP;
		public int SegCs;
		public int EFlags;
		public int Esp;
		public byte[] ExtendedRegisters=new byte[512];
	}*/
	public static class SYSTEM_INFO extends com.sun.jna.platform.win32.WinBase.SYSTEM_INFO {
		
	}
//	import com.sun.jna.Structure;

	/**
	 * Contains information about a range of pages in the virtual address space of a process. The VirtualQuery and VirtualQueryEx functions use this structure.
	 */
	//Base / Allocation address can be Pointer??
	public static class MemoryBasicInformation extends Structure {
	        public int BaseAddress;
	        public int AllocationBase;
	        public int AllocationProtect;
	        public int RegionSize;
	        public int State;
	        public int Protect;
	        public int Type;
	        @Override
	        protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[]{"baseAddress", "allocationBase", "allocationProtect",
	        "regionSize", "state", "protect", "type"});
	        }
	}
	

	//used in Module32First to retrieve information about the first module associated with a process.
	//used in Module32Next  to Retrieves information about the next module associated with a process or thread.
	
	public static class MODULEENTRY32 extends Structure {
		   public static class ByValue extends MODULEENTRY32 implements Structure.ByValue {
		    }

		    public static class ByReference extends MODULEENTRY32 implements Structure.ByReference {
		    }
		    public MODULEENTRY32() {
		        dwSize = size();
		    }
		    @Override
		    protected List<?> getFieldOrder() {
		        return Arrays.asList(new String[] { "dwSize", "th32ModuleID", "th32ProcessID", "GlblcntUsage", "ProccntUsage",
		                "modBaseAddr", "modBaseSize","hModule","szModule" ,"szExePath" });
		    }

		    public int dwSize;
		    public int th32ModuleID;
		    public int th32ProcessID;
		    public int GlblcntUsage;
		    public int ProccntUsage;
		    public int modBaseAddr;
		    public int modBaseSize;
		    public int hModule;
		    public char[] szModule = new char[256];
		    public char[] szExePath = new char[260];
	}
	
	public static class _MIB_TCPROW_OWNER_PID extends Structure {
		public static class ByValue extends _MIB_TCPROW_OWNER_PID implements Structure.ByValue {
	    }

	    public static class ByReference extends _MIB_TCPROW_OWNER_PID implements Structure.ByReference {
	    }
	    
	    @Override
	    protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] { "dwState", "dwLocalAddr", "dwLocalPort", "dwRemoteAddr", "dwRemotePort",
	                "dwOwningPid" });
	    }
	    public int dwState;
	    public int dwLocalAddr;
	    public int dwLocalPort;
	    public int dwRemoteAddr;
	    public int dwRemotePort;
	    public int dwOwningPid;
	    
	}
	
	public static class _MIB_UDPROW_OWNER_PID extends Structure {
		public static class ByValue extends _MIB_UDPROW_OWNER_PID implements Structure.ByValue {
	    }

	    public static class ByReference extends _MIB_UDPROW_OWNER_PID implements Structure.ByReference {
	    }
	    
	    @Override
	    protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] {"dwLocalAddr", "dwLocalPort",
	                "dwOwningPid" });
	    }
	    public int dwLocalAddr;
	    public int dwLocalPort;
	    public int dwOwningPid;
	    
	}
	
	public static class SYSDBG_MSR extends Structure{
		public static class ByValue extends SYSDBG_MSR implements Structure.ByValue {
	    }

	    public static class ByReference extends SYSDBG_MSR implements Structure.ByReference {
	    }
	    @Override
	    protected List<?> getFieldOrder() {
	        return Arrays.asList(new String[] {"Address", "Data" });
	    }
	    public long Address;
	    // Should be long long to fix;
	    public long  Data;
	    
	}
	
	// debug event codes.
	public static final int EXCEPTION_DEBUG_EVENT          = 0x00000001;
	public static final int CREATE_THREAD_DEBUG_EVENT      = 0x00000002;
	public static final int CREATE_PROCESS_DEBUG_EVENT     = 0x00000003;
	public static final int EXIT_THREAD_DEBUG_EVENT        = 0x00000004;
	public static final int EXIT_PROCESS_DEBUG_EVENT       = 0x00000005;
	public static final int LOAD_DLL_DEBUG_EVENT           = 0x00000006;
	public static final int UNLOAD_DLL_DEBUG_EVENT         = 0x00000007;
	public static final int OUTPUT_DEBUG_STRING_EVENT      = 0x00000008;
	public static final int RIP_EVENT                      = 0x00000009;
	public static final int USER_CALLBACK_DEBUG_EVENT      = 0xDEADBEEF ;    // added for callback support in debug event loop.

	// debug exception codes.
	public static final int EXCEPTION_ACCESS_VIOLATION     = 0xC0000005;
	public static final int EXCEPTION_BREAKPOINT           = 0x80000003;
	public static final int EXCEPTION_GUARD_PAGE           = 0x80000001;
	public static final int EXCEPTION_SINGLE_STEP          = 0x80000004;

	// hw breakpoint conditions
	public static final int HW_ACCESS                      = 0x00000003;
	public static final int HW_EXECUTE                     = 0x00000000;
	public static final int HW_WRITE                       = 0x00000001;
	//public static final int CONTEXT_CONTROL                = 0x00010001;
	//public static final int CONTEXT_FULL                   = 0x00010007;
	//public static final int CONTEXT_DEBUG_REGISTERS        = 0x00010010;
	public static final int CREATE_NEW_CONSOLE             = 0x00000010;
	public static final int DBG_CONTINUE                   = 0x00010002;
	public static final int DBG_EXCEPTION_NOT_HANDLED      = 0x80010001;
	public static final int DBG_EXCEPTION_HANDLED          = 0x00010001;
	public static final int DEBUG_PROCESS                  = 0x00000001;
	public static final int DEBUG_ONLY_THIS_PROCESS        = 0x00000002;
	public static final int EFLAGS_RF                      = 0x00010000;
	public static final int EFLAGS_TRAP                    = 0x00000100;
	public static final int ERROR_NO_MORE_FILES            = 0x00000012;
	public static int FILE_MAP_READ                  = 0x00000004;
	public static final int FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
	public static final int FORMAT_MESSAGE_FROM_SYSTEM     = 0x00001000;
	public static final int INVALID_HANDLE_VALUE           = 0xFFFFFFFF;
	public static final int MEM_COMMIT                     = 0x00001000;
	public static final int MEM_DECOMMIT                   = 0x00004000;
	public static final int MEM_IMAGE                      = 0x01000000;
	public static final int MEM_RELEASE                    = 0x00008000;
	public static final int PAGE_NOACCESS                  = 0x00000001;
	public static int PAGE_READONLY                  = 0x00000002;
	public static final int PAGE_READWRITE                 = 0x00000004;
	public static final int PAGE_WRITECOPY                 = 0x00000008;
	public static final int PAGE_EXECUTE                   = 0x00000010;
	public static final int PAGE_EXECUTE_READ              = 0x00000020;
	public static final int PAGE_EXECUTE_READWRITE         = 0x00000040;
	public static final int PAGE_EXECUTE_WRITECOPY         = 0x00000080;
	public static final int PAGE_GUARD                     = 0x00000100;
	public static final int PAGE_NOCACHE                   = 0x00000200;
	public static final int PAGE_WRITECOMBINE              = 0x00000400;
	public static final int PROCESS_ALL_ACCESS             = 0x001F0FFF;
	public static final int SE_PRIVILEGE_ENABLED           = 0x00000002;
	public static final int SW_SHOW                        = 0x00000005;
	//public static final int THREAD_ALL_ACCESS              = 0x001F03FF;
	public static final int THREAD_ALL_ACCESS              = 0x1fffff;
	public static final int TOKEN_ADJUST_PRIVILEGES        = 0x00000020;
	public static final int UDP_TABLE_OWNER_PID            = 0x00000001;
	public static final int VIRTUAL_MEM                    = 0x00003000;

	// for NtSystemDebugControl()
	public static final int SysDbgReadMsr                  = 16;
	public static final int SysDbgWriteMsr                 = 17;

	// for mapping TCP ports and PIDs
	public static final int AF_INET                        = 0x00000002;
	public static final int AF_INET6                       = 0x00000017;
	public static final int MIB_TCP_STATE_LISTEN           = 0x00000002;
	public static final int TCP_TABLE_OWNER_PID_ALL        = 0x00000005;
	
	
	public static final int CONTEXT_AMD64  = 0x100000;

	// end_wx86

	public static final int CONTEXT_CONTROL= (CONTEXT_AMD64 | 0x1);
	public static final int CONTEXT_INTEGER= (CONTEXT_AMD64 | 0x2);
	public static final int CONTEXT_SEGMENTS =(CONTEXT_AMD64 | 0x4);
	public static final int CONTEXT_FLOATING_POINT = (CONTEXT_AMD64 | 0x8);
	public static final int CONTEXT_DEBUG_REGISTERS =(CONTEXT_AMD64 | 0x10);

	public static final int CONTEXT_FULL =(CONTEXT_AMD64 | CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_FLOATING_POINT);

	public static final int CONTEXT_ALL =(CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS);

	public static final int CONTEXT_XSTATE =(CONTEXT_AMD64 | 0x20);

	public static final int CONTEXT_EXCEPTION_ACTIVE =0x8000000;
	public static final int CONTEXT_SERVICE_ACTIVE =0x10000000;
	public static final int CONTEXT_EXCEPTION_REQUEST =0x40000000;
	public static final int CONTEXT_EXCEPTION_REPORTING =0x80000000;
	
	
}
