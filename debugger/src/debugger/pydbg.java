package debugger;

import java.util.*;
import java.io.IOException;

import com.sun.jna.ptr.IntByReference;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import debugger.dbg.*;
import debugger.system_dll;
import diStorm3.CodeInfo;
import diStorm3.DecodedInst;
import diStorm3.DecodedResult;
import diStorm3.distorm3;
import diStorm3.distorm3.DecodeType;
import debugger.hook;
public class pydbg 
{
	int STRING_EXPLORATON_BUF_SIZE    = 256;
	int STRING_EXPLORATION_MIN_LENGTH = 2;
	public ArrayList restore_breakpoint = new ArrayList();

	//public ArrayList<system_dll> restore_breakpoint = new ArrayList<system_dll>();
	public Set<Integer> guarded_pages = new HashSet<Integer>();
	public Set<Integer> hlist;

	public boolean guard_active = true;
	public int page_size = 0;
	public int pid = 0;
	public Pointer h_process = null;
	public Pointer h_thread = null;
	public boolean debugger_active = true;
	public boolean follow_forks = true;
	public boolean client_server = true;
	
	// Class for exeption
	public Dictionary callbacks = new Hashtable();
	
	
	// system_dlls type not known 
	public ArrayList<system_dll> system_dlls = new ArrayList<system_dll>();
	public boolean dirty = true;
	public int system_break = 0;
	public byte peb;
	public Dictionary <Integer,Integer>tebs = new Hashtable<Integer,Integer>();
	
	public Context context;
	public DEBUG_EVENT dbg = null;
	public int exception_address = 0; 
	
	public long write_violation = 0;
	public long violation_address = 0;
	public int exception_code = 0;   // type not known
	
	public Dictionary<Integer,breakpoint> breakpoints = new Hashtable<Integer,breakpoint>();
	public Dictionary memory_breakpoints = new Hashtable();
	public Dictionary hardware_breakpoints = new Hashtable();
	
	public List memory_snapshot_block = new ArrayList();
	public List memory_snapshot_contexts = new ArrayList();

	public boolean first_breakpoint=true;
	
	public int memory_breakpoint_hit = 0;
	public int hardware_breakpoint_hit = 0;
	String instruction = null;
	String mnemonic = null;
	String op1=null;
	String op2 = null;
	String op3 = null;
	
	SYSTEM_INFO system_info = new SYSTEM_INFO();
//	public int page_size = 0;
	//public int 
	
	
	
	public pydbg(boolean ff,boolean cs)
	{
//		this.system_break = func_resolve("ntdll.dll", "DbgBreakPoint");
		this.follow_forks=ff;
		this.client_server=cs;
		Kernel32.GetSystemInfo(this.system_info);
		
	}
	
	
	
	public system_dll addr_to_dll(int address)
	{
		for (system_dll dll : this.system_dlls)
		{
			if ((dll.base < address) && (address < dll.base + dll.size))
                return dll;
		}
		return null;
	}
	
	
	// Need to complete later
	/*public MODULEENTRY32 addr_to_module(int address)
	{
		MODULEENTRY32 found = null;
		for()
	}*/
	
	
	public pydbg attach(int pid)
	{
		this.get_debug_privileges();
		this.pid=pid;
		this.open_process(pid);
		this.debug_active_process(pid);
		try
		{
			this.debug_set_process_kill_on_exit(false);
		}
		catch(Exception e)
		{
		}
		ArrayList <Integer>enumerate_thread = new ArrayList<Integer>();
		enumerate_thread = this.enumerate_threads();
		Pointer thread_handle;
		Context thread_context;
		LDT_ENTRY selector_entry;
		for(int i : enumerate_thread)
		{
			thread_handle = this.open_thread(i);
			thread_context = this.get_thread_context(thread_handle,0);
		    selector_entry = new LDT_ENTRY();
		    int teb = selector_entry.BaseLow;
			teb+=(selector_entry.HighWord.Bits.BaseMid << 16) + (selector_entry.HighWord.Bits.BaseHi << 24);
			this.tebs.put(i,teb);
			if(this.peb!=0)
			{
				//to uncomment
			this.peb = this.read_process_memory((teb+0x30),4);
			}
		}
		return this;
		
	}
	
	
	public pydbg bp_set(int address,String description,boolean restore,Pointer handler)
	{
		System.out.println("Entered");
		
		byte original_byte = (this.read_process_memory(address,1));
		System.out.println(original_byte);
		byte data=(byte)0xCC;
		String tmp;
		tmp=Byte.toString(data);
		//System.out.println("Initail Data:"+tmp+ "Address: "+Integer.toHexString(address));
		this.write_process_memory(address,data,1);
		this.dirty=true;
		System.out.println("1st" + " address "+ address+ " Address: "+Integer.toHexString(address));
		breakpoint A = new breakpoint(address, original_byte, description, restore, handler);
		//System.out.println("2nd");
		this.breakpoints.put(address,A);
		return this;
	}
	
	public byte read_process_memory (int address,int length)
	{
	        String data = "";
	        StringBuffer read_buf = new StringBuffer(length);
	        int count = 0;
	        int orig_length  = length;
	        int orig_address = address;

	        // ensure we can read from the requested memory space.
	        int _address = address;
	        int _length  = length;
	        int old_protect=0;
	 //       System.out.println("Virtual_Protect_Enter");
	        try
	        {
	            old_protect = this.virtual_protect(_address, _length, debugger.dbg.PAGE_EXECUTE_READWRITE);
	        }
	        catch(Exception e)
	        {
	        	
	        }
	   //     System.out.println(old_protect);
	    //    System.out.println("Virtual_Protect_left");
	        String tmp="";
	        Pointer cnt=new Pointer(0);
	      //  cnt=cnt.getPointer(2);
	        char [] tmp1=new char[100];
	    //    Pointer ad=(Pointer)address;
	       // cnt.setInt(0, 0);
	     	IntByReference read = new IntByReference(0);
        	Memory output = new Memory(length);
	      //     System.out.println(output.toString());
	        while(length>0)
	        {
	       //	System.out.println(length);
	        //	System.out.println(tmp);
	        	//System.out.println(this.h_process);
	   
	            if(!Kernel32.ReadProcessMemory(this.h_process, address, output, length, read))
	            {
	           // 	 System.out.println(String.format("Here:  %02X ", output.getByte(0)));
	            	return output.getByte(0);
	            }
	            data    += String.format("%02X ", output.getByte(0));
	            length  -= read.getValue();
	      //     System.out.println(read.getValue());
	        //   System.out.println(output.toString());
	   //         address += cnt.nativeValue(cnt);
	        }
	        // restore the original page permissions on the target memory region.
	        try{
	            this.virtual_protect(_address, _length, old_protect);
	        }
	        catch(Exception e)
	        {
	        	
	        }
	     //   System.out.println(String.format("2nd here :%02X ", output.getByte(0)) + " " + data);
	        return output.getByte(0);

	}
	
	public void write_process_memory(int address,byte data,int length)
	{
		System.out.println("ENtered Write"+ Integer.toHexString(address));
		int count = 0;
		length = 1;
		int _address=address;
		int _length = length;
		int old_protect=0;
		try
		{
			old_protect = this.virtual_protect(_address, _length,debugger.dbg.PAGE_EXECUTE_READWRITE);
		}
		catch(Exception e)
		{
			
		}
	//	System.out.println("data::: " +data);
		String tmp="CC";
		//System.out.println(String.format("tmp::: %02X ",tmp.getBytes()[0]));
		//System.out.println(String.format("%02X ",data.getBytes()[0]));
	//	byte[] a=data.getBytes();
		IntByReference written =  new IntByReference(0);
		Memory toWrite = new Memory(1);
		/*for(long i = 0; i < a.length;i++)
        {
			toWrite.setByte(0, a[new Integer(Long.toString(i))]);
			 System.out.println(String.format("%02X ",a[new Integer(Long.toString(i))]));
          //  toWrite.setShort(0, data[new Integer(Long.toString(i))]);
        }
		length=a.length;*/
		toWrite.setByte(0, data);
		while(length>0)
		{
		//	String c_data="";
			//for(int i=count;i<data.length();i++)
			//{
				//c_data+=data.charAt(i);
			//}
			System.out.println(String.format("Byte written: %02X ", toWrite.getByte(0))+ "Address: " +Integer.toHexString(address));
			Kernel32.WriteProcessMemory(this.h_process, address, toWrite, length, written);
			length -= written.getValue();
			System.out.println("length: "+length);
		//	address += count;
		}
		try
		{
			this.virtual_protect(_address, _length, old_protect);
		}
		catch(Exception e)
		{
			
		}
//		System.out.println("Left write memory");
	}
	public int virtual_protect(int base_address,int size,int protection)
	{
		int old_protect = 0;
		Pointer old=new Pointer(0);
		IntByReference read = new IntByReference(0);
		Kernel32.VirtualProtectEx(this.h_process, base_address, size, protection, read);
//		System.out.println(Kernel32.VirtualProtectEx(this.h_process, base_address, size, protection, read));
	//	System.out.println("val:-");
	//	System.out.println(read.getValue());
		return read.getValue();
	}
	public void run()
	{
		this.debug_event_loop();
	}
	
	public void debug_event_loop()
	{
		while(this.debugger_active)
		{
			try
			{
				
			}
			catch(Exception e)
			{
				
			}
			if(this.callbacks.get(debugger.dbg.USER_CALLBACK_DEBUG_EVENT)!=null)
			{
				this.dbg=null;
				this.context=null;
	//			(this.callbacks.get(debugger.dbg.USER_CALLBACK_DEBUG_EVENT))();
			}
			this.debug_event_iteration();
		}
		this.close_handle(this.h_process);

	}
	public Pointer open_process(int pid)
	{
		this.h_process=Kernel32.OpenProcess(debugger.dbg.PROCESS_ALL_ACCESS, false, pid);
		return this.h_process;
	}
	
	//To do
	public void get_debug_privileges()
	{
		
	}
	public void debug_active_process(int pid)
	{
		boolean b;
		b=Kernel32.DebugActiveProcess(pid);
	    System.out.println("Debug_Active_process :: "+ b);
	}
	public void debug_set_process_kill_on_exit(boolean kill_on_exit)
	{
		Kernel32.DebugSetProcessKillOnExit(kill_on_exit);
		
	}
	public ArrayList<Integer> enumerate_threads()
	{
		ArrayList<Integer> debuggee_threads = new ArrayList<Integer>();
		THREADENTRY32 thread_entry=new THREADENTRY32();
		Pointer snapshot;
		snapshot=Kernel32.CreateToolhelp32Snapshot(debugger.dbg.TH32CS_SNAPTHREAD, this.pid);
	//	if(snapshot.getInt(0)==debugger.dbg.INVALID_HANDLE_VALUE)
	//	{
		//	//to write
	//		
		//}
		thread_entry.dwSize=thread_entry.size();
		boolean success = Kernel32.Thread32First(snapshot,thread_entry);
		while (success)
		{
			if (thread_entry.th32OwnerProcessID == this.pid)
			{
				debuggee_threads.add(thread_entry.th32ThreadID);
			}
			success = Kernel32.Thread32Next(snapshot,thread_entry);
		}
		this.close_handle(snapshot);
        return debuggee_threads;		
	}
	
	public boolean close_handle(Pointer handle)
	{
		return Kernel32.CloseHandle(handle);
	}
	
	public Pointer open_thread(int thread_id)
	{
		Pointer h_thread=null;
		h_thread= Kernel32.OpenThread(debugger.dbg.THREAD_ALL_ACCESS, false, thread_id);
		System.out.println("thread id:  "+thread_id);
		if(h_thread==null)
			System.out.println("Thread NUll");
		return h_thread;
	}
	
	public Context get_thread_context(Pointer thread_handle,int thread_id)
	{
		Pointer h_thread;
		Context context=new Context();
		context.ContextFlags = debugger.dbg.CONTEXT_FULL | debugger.dbg.CONTEXT_DEBUG_REGISTERS;
		if (thread_handle==null)
            h_thread = this.open_thread(thread_id);
        else
            h_thread = thread_handle;
		System.out.println(" suspend : " + Kernel32.SuspendThread(h_thread));
		boolean tmp=Kernel32.GetThreadContext(h_thread, context);
		System.out.println("Context:: "+tmp+" "+Integer.toHexString(context.Rip)+ " "+ Long.toHexString(context.Rsp)+ " " + context.Rip + " "+ context.Rsp);
		Kernel32.ResumeThread(h_thread);
		if (thread_handle==null)
			this.close_handle(h_thread);
	 
		return context;

		
	}
	
	public int func_resolve(String dll, String function)
	{
		Pointer handle;
		int address;
		handle  = Kernel32.LoadLibraryA(dll);
		address = Kernel32.GetProcAddress(handle, function);
		Kernel32.FreeLibrary(handle);
	    return address;
	}
	
	public void debug_event_iteration()
	{
		int continue_status = debugger.dbg.DBG_CONTINUE;
//		System.out.println("debug_event_iteration");
		DEBUG_EVENT.ByReference dbg = new DEBUG_EVENT.ByReference();
		dbg.u=new u_union();
		//dbg.u.setType("Exception");
//		dbg.u.setType("Exception")
		dbg.u.Exception.dwFirstChance=100;
		boolean debug=Kernel32.WaitForDebugEvent(dbg, 100);
		
		if(debug)
		{
			System.out.println("debug:: "+ debug);
			EXIT_THREAD_DEBUG_INFO Exception1=(EXIT_THREAD_DEBUG_INFO)dbg.u.getTypedValue(EXIT_THREAD_DEBUG_INFO.class);
			//EXCEPTION_DEBUG_INFO Exception1=(EXCEPTION_DEBUG_INFO)dbg.u.readField("Exception");
	//		System.out.println(Exception1.dwExitCode+"  dont know  "+dbg.u.ExitThread.dwExitCode+" code: "+dbg.u.Exception.dwFirstChance);
			Scanner sc =new Scanner(System.in);
		//	this.h_thread = 		 this.open_thread(sc.nextInt());
//			sc.close();
			this.h_thread = 		 this.open_thread(dbg.dwThreadId);
			this.context           = this.get_thread_context(this.h_thread,0);
		    this.dbg               = dbg;
		    this.exception_address = dbg.u.Exception.ExceptionRecord.ExceptionAddress;
		    this.write_violation   = dbg.u.Exception.ExceptionRecord.ExceptionInformation[0];
		    this.violation_address = dbg.u.Exception.ExceptionRecord.ExceptionInformation[1];
		    this.exception_code    = dbg.u.Exception.ExceptionRecord.ExceptionCode;
		   // System.out.println("Addr_ex::"+this.exception_address);
		    if(dbg.dwDebugEventCode == debugger.dbg.CREATE_PROCESS_DEBUG_EVENT)
		    {
		    	System.out.println("1");
		    //	continue_status = this.event_handler_create_process();
		    }
		    else if(dbg.dwDebugEventCode == debugger.dbg.CREATE_THREAD_DEBUG_EVENT)
		    {
		    	System.out.println("2");

		    	//continue_status = this.event_handler_create_thread();
		    }
		    else if(dbg.dwDebugEventCode == debugger.dbg.EXIT_PROCESS_DEBUG_EVENT)
		    {
		    	System.out.println("3");

            //    continue_status = this.event_handler_exit_process();
		    }
		    else if( dbg.dwDebugEventCode == debugger.dbg.EXIT_THREAD_DEBUG_EVENT)
		    {
		    	System.out.println("4");

                //continue_status = this.event_handler_exit_thread();
		    }
		    else if(dbg.dwDebugEventCode == debugger.dbg.LOAD_DLL_DEBUG_EVENT)
		    {
		    	System.out.println("5");

               // continue_status = this.event_handler_load_dll();
		    }
		    else if(dbg.dwDebugEventCode == debugger.dbg.UNLOAD_DLL_DEBUG_EVENT)
		    {
		    	System.out.println("6");

             //   continue_status = this.event_handler_unload_dll();
		    }
		    else if(dbg.dwDebugEventCode == debugger.dbg.EXCEPTION_DEBUG_EVENT)
		    {
		    //	this.h_thread = 		 this.open_thread(dbg.dwThreadId);
		//		this.context           = this.get_thread_context(this.h_thread,0);
		    	Context context = this.get_thread_context(this.h_thread,0);
		    	EXCEPTION_DEBUG_INFO Exception2=(EXCEPTION_DEBUG_INFO)dbg.u.getTypedValue(EXCEPTION_DEBUG_INFO.class);
		    	this.exception_address = Exception2.ExceptionRecord.ExceptionAddress;
			    this.write_violation   = Exception2.ExceptionRecord.ExceptionInformation[0];
			    this.violation_address = Exception2.ExceptionRecord.ExceptionInformation[1];
			    this.exception_code    = Exception2.ExceptionRecord.ExceptionCode;
		    	System.out.println(Integer.toHexString(this.exception_address)+" "+this.write_violation+" "+this.violation_address+" "+Long.toHexString(context.Rip)+" "+Long.toHexString(context.Rip)+" "+Integer.toHexString(Exception2.ExceptionRecord.ExceptionAddress));
                int ec = dbg.u.Exception.ExceptionRecord.ExceptionCode;
                System.out.println("7 "+ Integer.toHexString(ec)+" " +Integer.toHexString(this.exception_address)+" "+Integer.toHexString(this.exception_code)+" "+this.h_thread.nativeValue(h_thread));
                this.exception_address=dbg.u.Exception.ExceptionRecord.ExceptionAddress;
                // call the internal handler for the exception event that just occured.
                if(ec == debugger.dbg.EXCEPTION_ACCESS_VIOLATION)
                {
                	System.out.println("Access Violation");
    //                continue_status = this.exception_handler_access_violation();
                }
                else if(ec == debugger.dbg.EXCEPTION_BREAKPOINT)
                {
                	System.out.println("BreakPoint Violation");
  //              	this.exception_handler_breakpoint();
                	continue_status = this.exception_handler_breakpoint();
                }
                else if(ec == debugger.dbg.EXCEPTION_GUARD_PAGE)
                {
                	System.out.println("Gaurd Page Violation");

        //            continue_status = this.exception_handler_guard_page();
                }
                else if(ec == debugger.dbg.EXCEPTION_SINGLE_STEP)
                {
                	System.out.println("Single Violation");
          //          continue_status = this.exception_handler_single_step();
                }
                else if(ec==0x4000001f)
                {
                	System.out.println("BreakPoint Violation");
                	print_instr(this.exception_address);
                	//this.exception_handler_breakpoint();
                   	continue_status = this.exception_handler_breakpoint();
                   	Scanner scan =new Scanner(System.in);
                   	System.out.println("You typed " + scan.next());
                   	scan.close();
                   	
                }
                // generic callback support.
            //    else if(this.callbacks.keys(ec)!=null)
              //  {
                //    continue_status = this.callbacks[ec]();
                //}
                // unhandled exception.
                else
                {
    		    	System.out.println("8");

                    continue_status = debugger.dbg.DBG_EXCEPTION_NOT_HANDLED;
                }
		    }
		    else
		    {
		   // 	System.out.println("9");

		    }
            // if the memory space of the debuggee was tainted, flush the instruction cache.
            // from MSDN: Applications should call FlushInstructionCache if they generate or modify code in memory.
            //           The CPU cannot detect the change, and may execute the old code it cached.
            if(this.dirty)
            {
                Kernel32.FlushInstructionCache(this.h_process, null, 0);
            }

            // close the opened thread handle and resume executing the thread that triggered the debug event.
            this.close_handle(this.h_thread);
            Kernel32.ContinueDebugEvent(dbg.dwProcessId, dbg.dwThreadId, continue_status);
		}
	}

	public int exception_handler_breakpoint()
	{
		//if(this.breakpoints.keys()
	//	System.out.println("entered1");
		hlist=hook.hook_list;
		for(int i : hlist)
		{
			if(this.exception_address==i)
			{
				hook.hook_start();
			}
		}
	
		breakpoint tmp=this.breakpoints.get(this.exception_address);
		if(tmp!=null)
		{
	//		System.out.println(this.exception_address+ " "+breakpoints.get(this.exception_address)+  " Address: "+Integer.toHexString(this.exception_address));
		//	System.out.println("entered2  "+ Integer.toHexString(this.exception_address));
			//System.out.println(this.exception_address + " "+ tmp.original_byte);
			this.write_process_memory(this.exception_address, tmp.original_byte,1);
			this.dirty=true;
			this.set_register("Eip", this.exception_address);
			this.context.Rip -= 1;
			//this.single_step(true,this.h_thread);
		}
		return debugger.dbg.DBG_CONTINUE;
	}
	public pydbg single_step(boolean enable,Pointer thread_handle)
	{
		Context context = this.get_thread_context(thread_handle,0);
		if(enable)
		{
			int tmp=context.EFlags & debugger.dbg.EFLAGS_TRAP;
			if (tmp!=0)
			{
				return this;
			}
			context.EFlags |= debugger.dbg.EFLAGS_TRAP;
		}
		else
		{
			int tmp=context.EFlags & debugger.dbg.EFLAGS_TRAP;
			if(tmp==0)
			{
				return this;
			}
			context.EFlags = context.EFlags & (0xFFFFFFFF ^ debugger.dbg.EFLAGS_TRAP);
		}
		this.set_thread_context(context, thread_handle,0);
		
		return this;
	}
	public pydbg set_register(String register,int value)
	{
		Context context = this.get_thread_context(this.h_thread,0);
		System.out.println("register "+register);
		if(register.equals("Eax"))
		{
			context.Rax = value;
		}
		else if(register.equals("Ebx"))
		{
			context.Rbx = value;
		}
		else if(register.equals("Ecx"))
		{
			context.Rcx = value;
		}
		else if(register.equals("Edx"))
		{
			context.Rdx = value;
		}
		else if(register.equals("Esi"))
		{
			context.Rsi = value;
		}
		else if(register.equals("Edi"))
		{
			context.Rdi = value;
		}
		else if(register.equals("Esp"))
		{
			context.Rsp = value;
		}
		else if(register.equals("Ebp"))
		{
			context.Rbp = value;
		}
		else if(register.equals("Eip"))
		{
			System.out.println("EIP:: "+Integer.toHexString(context.Rip)+ " value: "+Integer.toHexString(value));
			context.Rip = value;
			System.out.println("FEIP:: "+Integer.toHexString(context.Rip)+ " Fvalue: "+Integer.toHexString(value));

		}
		this.set_thread_context(context,this.h_thread,0);
		return this;
	}
	
	public pydbg set_thread_context(Context context ,Pointer thread_handle,int thread_id)
	{
		Pointer h_thread;
		//context.ContextFlags = debugger.dbg.CONTEXT_FULL | debugger.dbg.CONTEXT_DEBUG_REGISTERS;
		if (thread_handle==null)
            h_thread = this.open_thread(thread_id);
        else
            h_thread = thread_handle;
		Kernel32.SetThreadContext(h_thread, context);
		if (thread_handle==null)
			this.close_handle(h_thread);
		return this;

		
	}
	public void print_instr(int address)
	{
		int temp=address;
	//	byte original_byte = (this.read_process_memory(address,1));
		int i;
		byte[] data= new byte[200];
		for(i=0;i<200;i++)
		{
			data[i]=this.read_process_memory(temp, 1);
			temp++;
		}
		
		CodeInfo ci = new CodeInfo((long)address,data, DecodeType.Decode32Bits, 0);
		DecodedResult dr = new DecodedResult(200);
		distorm3.Decode(ci, dr);

		for (DecodedInst x : dr.mInstructions) {
			String s = String.format("%x %s %s", x.getOffset(), x.getMnemonic(), x.getOperands());
			System.out.println(s);
		}
	}
}
