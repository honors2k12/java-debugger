package debugger;
import com.sun.jna.Pointer;
import java.util.Random;
import debugger.dbg.MemoryBasicInformation;
public class memory_breakpoint {

	public int address;
	public int size;
	public MemoryBasicInformation mbi=new MemoryBasicInformation();
	public String description;
	public Pointer handler;
	public int read_count=0;
	public int split_count=0;
	public int copy_depth=0;
	public int id=0;
	public boolean on_stack=false;
	
	public memory_breakpoint(int address, int size , MemoryBasicInformation mbi,String description,Pointer handler)
	{
		this.address=address;
		this.size=size;
		this.mbi=mbi;
		this.description=description;
		this.handler=handler;
		this.id=(new Random()).nextInt(0xFFFFFFFF+1);
		this.read_count=0;
		this.split_count=0;
		this.copy_depth=0;
		this.on_stack=false;
	}
}
