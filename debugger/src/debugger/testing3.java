package debugger;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.ptr.IntByReference;

import debugger.pydbg;
import debugger.dbg.*;
import debugger.system_dll;
import diStorm3.CodeInfo;
import diStorm3.DecodedInst;
import diStorm3.DecodedResult;
import diStorm3.distorm3;
import diStorm3.distorm3.DecodeType;

import java.util.Scanner;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
public class testing3 {
	static class Kernel32
	{
	     static
	     {
	          Native.register("kernel32");
	     }
	     public static native Pointer OpenProcess(int dwDesiredAccess, boolean bInheritHandle, int dwProcessId);
	     public static native boolean CloseHandle(Pointer hObject);
	 //    public static native boolean GetFileInformationByHandle(
	   //           Pointer hFile,
	     //         BY_HANDLE_FILE_INFORMATION lpFileInformation
	       //     );
	     public static native int  GetProcessId(  Pointer Process);
	     public static native boolean GetProcessTimes(Pointer handle, FILETIME lpCreationTime, FILETIME lpExitTime, FILETIME lpKernelTime, FILETIME lpUserTime);
	     public static native void GetSystemInfo(SYSTEM_INFO lpSystemInfo);
	     public static native boolean ReadProcessMemory(Pointer hProcess,int lpBaseAddress,Pointer lpBuffer,int nSize,IntByReference lpNumberOfBytesRead);
	     //To change above later
	     public static native boolean WriteProcessMemory(Pointer hProcess,int lpBaseAddress,Pointer lpBuffer,int nSize,IntByReference lpNumberOfBytesRead);
	     //To change above later
	     public static native boolean VirtualProtectEx(Pointer hProcess,int lpAddress,int dwSize,int flNewProtect,IntByReference lpflOldProtect);
	     public static native boolean DebugActiveProcess(int dwProcessId);
	     public static native boolean DebugSetProcessKillOnExit(boolean kill_on_exit);
	     public static native Pointer CreateToolhelp32Snapshot(int dwFlags,int th32ProcessID);
	     public static native boolean Thread32First(Pointer hSnapshot,THREADENTRY32 lpte);
	     public static native boolean Thread32Next(Pointer hSnapshot,THREADENTRY32 lpte);
	     public static native Pointer OpenThread(int dwDesiredAccess,boolean bInheritHandle,int dwThreadId);
	     public static native boolean GetThreadContext(Pointer hThread,Context lpContext);
	     public static native boolean SetThreadContext(Pointer hThread,Context lpContext);
	     public static native Pointer LoadLibraryA(String lpFileName);
	     public static native int GetProcAddress(Pointer hModule,String lpProcName);
	     public static native boolean FreeLibrary(Pointer hModule);
	     public static native boolean ContinueDebugEvent(int dwProcessId,int dwThreadId,int dwContinueStatus);
	     public static native boolean FlushInstructionCache(Pointer hProcess,Pointer lpBaseAddress,int dwSize);
	     public static native boolean WaitForDebugEvent(DEBUG_EVENT lpDebugEvent,int dwMilliseconds);
	     public static native boolean SuspendThread(Pointer h_thread);
	     public static native boolean ResumeThread(Pointer h_thread);
	     public static native int GetLastError();
	     public static native void ZeroMemory(Pointer a,int length);
	}
	 public static  void main(String[] args)
	    {
		 Scanner a =new Scanner(System.in);
		 int val=a.nextInt();
		 Pointer hThread = Kernel32.OpenThread(dbg.THREAD_ALL_ACCESS,false,val);

				    if(hThread  == null) {
				        System.out.println("Error opening thread handle.."+ Integer.toHexString(Kernel32.GetLastError()));
				        return ;
				    }   

				    // suspend the thread
				    if(Kernel32.SuspendThread(hThread )) {
				    	System.out.println("Error suspending thread.. "+ Integer.toHexString(Kernel32.GetLastError()));
				        Kernel32.CloseHandle(hThread );
				        return ;
				    }

				    // get the thread context
				    Context orig_ctx1=null;
		//		    Kernel32.ZeroMemory(orig_ctx1.getPointer(), orig_ctx1.size());
				    orig_ctx1.ContextFlags = dbg.CONTEXT_FULL;
				    if(Kernel32.GetThreadContext(hThread , orig_ctx1) == false) {
				    	System.out.println("Error  "+ Integer.toHexString(Kernel32.GetLastError()));
				    	Kernel32.CloseHandle(hThread );
				        return;
				    }
	    }
}
