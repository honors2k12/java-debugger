package debugger;
//used to work
//import win.test.createprocess.BY_HANDLE_FILE_INFORMATION;
import debugger.dbg;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinBase.FILETIME;
import com.sun.jna.platform.win32.WinBase.PROCESS_INFORMATION;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
import com.sun.jna.platform.win32.WinBase.STARTUPINFO;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinBase.SECURITY_ATTRIBUTES;
//import com.sun.jna.platform.win32.Kernel32;

public class system_dll {

	static class Kernel32
	{
	     static
	     {
	          Native.register("kernel32");
	     }
	 /*    public static native Pointer OpenProcess(int dwDesiredAccess, boolean bInheritHandle, int dwProcessId);
	     public static native boolean CloseHandle(Pointer hObject);
	     public static native boolean GetFileInformationByHandle(
	              Pointer hFile,
	              BY_HANDLE_FILE_INFORMATION lpFileInformation
	            );
	     public static native int  GetProcessId(  Pointer Process);
	     public static native boolean GetProcessTimes(Pointer handle, FILETIME lpCreationTime, FILETIME lpExitTime, FILETIME lpKernelTime, FILETIME lpUserTime);
	     public static native int CreateProcessA(String lpApplicationName,String lpCommandLine,SECURITY_ATTRIBUTES lpProcessAttributes,SECURITY_ATTRIBUTES lpThreadAttributes,boolean bInheritHandles,int dwCreationFlags, Pointer lpEnvironment , String lpCurrentDirectory , STARTUPINFO lpStartupInfo , PROCESS_INFORMATION lpProcessInformation);
	     public static native int WaitForSingleObject( HANDLE hHandle,int dwMilliseconds);
	     //public static native boolean CloseHandle(HANDLE hObject);
	*/
	     public static native int GetFileSize(HANDLE handle, Pointer file_size_hi);
	     public static native HANDLE CreateFileMappingA(HANDLE handle,SECURITY_ATTRIBUTES lpAttributes,int flProtect,int dwMaximumSizeHigh,int dwMaximumSizeLow,String lpName);
	     public static native Pointer MapViewOfFile(HANDLE hFileMappingObject,int dwDesiredAccess,int dwFileOffsetHigh,int dwFileOffsetLow,int dwNumberOfBytesToMap);
	     public static native HANDLE GetCurrentProcess();
	     public static native boolean UnmapViewOfFile(Pointer lpBaseAddress);
	     public static native boolean CloseHandle(HANDLE hObject);
	     //boolean a;
	     
	}
	static class Psapi
	{
	     static
	     {
	          Native.register("Psapi");
	     }
	 //    public static native boolean EnumProcesses(int[] ProcessIDsOut, int size, int[] BytesReturned);
	   //  public static native int GetModuleBaseNameW(Pointer hProcess, Pointer hModule, byte[] lpBaseName, int nSize);
	     public static native int GetMappedFileNameA(HANDLE hProcess,Pointer lpv,String lpFilename,int nSize);
	}
	public HANDLE handle;
	public int base;
	public String name;
	public String path;
	public String pe;
	public int size;
	public system_dll(HANDLE handle, int base)
	{
		this.handle=handle;
		this.base=base;
		this.name=null;
		this.path=null;
		this.pe=null;
		this.size=0;
		int file_size_hi=0;
		int file_size_lo =0;
		Pointer P=new Pointer(file_size_hi);
		P.setInt(0, file_size_hi);
		file_size_lo=Kernel32.GetFileSize(handle, P);
		this.size=(P.getInt(0)<<8)+file_size_lo;
		HANDLE file_map;
		file_map = Kernel32.CreateFileMappingA(handle, null, dbg.PAGE_READONLY, 0, 1, null);
		if (file_map!=null)
		{
			Pointer file_ptr;
			file_ptr=Kernel32.MapViewOfFile(file_map, dbg.FILE_MAP_READ, 0, 0, 1);
			if(file_ptr!=null)
			{
	//			String filename;
				char[] filename=new char[2048];
				Psapi.GetMappedFileNameA(Kernel32.GetCurrentProcess(), file_ptr, filename.toString(), 2048);
				
				Kernel32.UnmapViewOfFile(file_ptr);
			}
			Kernel32.CloseHandle(file_map);
		}
	}
	
	public void del()
	{
		Kernel32.CloseHandle(this.handle);
	}
}
