package debugger;
import debugger.dbg.Context;
public class memory_snapshot_context {

	public int thread_id;
	public Context context;
	
	public memory_snapshot_context(int thread_id,Context context)
	{
		this.thread_id=thread_id;
		this.context=context;
	}
}
