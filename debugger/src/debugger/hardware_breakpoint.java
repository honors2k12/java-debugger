package debugger;
import debugger.dbg;
import debugger.dbg.THREADENTRY32;
import com.sun.jna.Pointer;

public class hardware_breakpoint {
//	int a=dbg.PROCESS_VM_READ;
//	THREADENTRY32 ab = new THREADENTRY32();

	public  int address;
	public  int length;
	public  int condition;
	public  boolean restore;
	public  int slot;
	public  Pointer handler;
	
	public hardware_breakpoint(int address,int length,int condition,boolean restore,int slot,Pointer handler)
	{
		this.address=address;
		this.length=length;
		this.condition=condition;
		this.restore=restore;
		this.slot=slot;
		this.handler=handler;
	}
}
