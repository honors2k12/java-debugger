package debugger;
import debugger.pydbg;
import debugger.hook;

import java.util.Scanner;

import com.sun.jna.Pointer;
public class testing {
			public static void main(String args[])
			{
				Pointer cnt=new Pointer(2);
			       // cnt.setInt(0, 0);
				System.out.println(cnt.nativeValue(cnt));
				Scanner a =new Scanner(System.in);
				int pid=a.nextInt();
				pydbg debugger=new pydbg(false,false);
				debugger.attach(pid);
				System.out.println("Attached successfully");
				int printf_address = debugger.func_resolve("MSVCRT.dll","printf");
				System.out.println("[*] Address of printf: 0x" + Integer.toHexString(printf_address));
				
				printf_address = debugger.func_resolve("process1","test");
				System.out.println("[*] Address of printf: 0x" + Integer.toHexString(printf_address));
				
				printf_address=0x7714F91D;
				
				debugger=hook.address_hooking(printf_address, debugger);
				//debugger.bp_set(printf_address,null,false,null);
				System.out.println("returned");
				debugger.run();
				System.out.println("finished");

				a.close();
			}
}
