package debugger;

import java.util.*;
import java.io.IOException;

import debugger.pydbg;

public class hook {

	public static Set<Integer> hook_list = new HashSet<Integer>();
	public static pydbg address_hooking(int address,pydbg debug)
	{
		debug.bp_set(address,null,false,null);
		hook_list.add(address);
		return debug;
	}
	
	public static Set<Integer> get_hook_list()
	{
		return hook_list;
	}
	public static void hook_start()
	{
		System.out.println("function hooked");
	}
	
}
