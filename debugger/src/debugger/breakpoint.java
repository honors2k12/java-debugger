package debugger;
import com.sun.jna.Pointer;
import java.util.*;

public class breakpoint {

	public int address;
	public byte original_byte;
	public String description;
	public boolean restore;
	public Pointer handler;
	
	
	public breakpoint(int address,byte original_byte,String description,boolean restore,Pointer handler)
	{
		this.address=address;
		this.original_byte=original_byte;
		this.description=description;
		this.restore=restore;
		this.handler=handler;
	}
}
